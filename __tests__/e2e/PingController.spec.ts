import supertest from "supertest";
import app from "../../src/app";

describe("PingController", () => {
    test("Should return pong equals true when the ping endpoint was called", async () => {

        const response = await supertest(app).get("/v1/ping").expect("Content-Type", /json/).expect(200);

        expect(response.body).toEqual({ pong: true });
    });
});