import * as crypto from "crypto";
import DeliveryMan from "../../src/domain/DeliveryMan";

describe("DeliveryMan", () => {
    test("Should create a DeliveryMan object correctly", () => {

        const deliveryManId: string = crypto.randomUUID();
        const deliveryManName: string = crypto.randomBytes(15).toString("hex");
        const deliveryManEmail: string = crypto.randomBytes(30).toString("hex");
        const deliveryManIsEnabled: boolean = true;

        let deliveryMan: DeliveryMan = new DeliveryMan();
        deliveryMan.id = deliveryManId;
        deliveryMan.name = deliveryManName;
        deliveryMan.email = deliveryManEmail;
        deliveryMan.isEnabled = deliveryManIsEnabled;

        expect(deliveryMan).toBeInstanceOf(DeliveryMan);
        expect(deliveryManId).toStrictEqual(deliveryMan.id);
        expect(deliveryManName).toStrictEqual(deliveryMan.name);
        expect(deliveryManEmail).toStrictEqual(deliveryMan.email);
        expect(deliveryManIsEnabled).toStrictEqual(deliveryMan.isEnabled);
    })
});