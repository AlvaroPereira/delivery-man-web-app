import * as crypto from "crypto";
import Establishment from "../../src/domain/Establishment";

describe("Establishment", () => {
    test("Should create a Establishment object correctly", () => {

        const establishmentId: string = crypto.randomUUID();
        const establishmentName: string = crypto.randomBytes(15).toString("hex");
        const establishmentDescription: string = crypto.randomBytes(20).toString("hex");
        const establishmentIsEnabled: boolean = true;

        let establishment: Establishment = new Establishment();
        establishment.id = establishmentId;
        establishment.name = establishmentName;
        establishment.description = establishmentDescription;
        establishment.isEnabled = establishmentIsEnabled;

        expect(establishment).toBeInstanceOf(Establishment);
        expect(establishmentId).toStrictEqual(establishment.id);
        expect(establishmentName).toStrictEqual(establishment.name);
        expect(establishmentDescription).toStrictEqual(establishment.description);
        expect(establishmentIsEnabled).toStrictEqual(establishment.isEnabled);
    })
});