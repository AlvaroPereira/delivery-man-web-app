export default class DeliveryMan {
    public id: string;
    public name: string;
    public email: string;
    public isEnabled: boolean;
}