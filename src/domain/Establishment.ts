export default class Establishment {
    public id: string;
    public name: string;
    public description: string;
    public isEnabled: boolean;
}