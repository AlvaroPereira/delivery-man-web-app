import { Request, Response } from "express";

export default class PingController {
    static index(request: Request, response: Response): Response<any, Record<string, any>> {
        return response.status(200).json({pong: true});
    }
}