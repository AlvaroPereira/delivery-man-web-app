import express from "express";
import PingController from "./presentation/controllers/PingController";
const router = express.Router();

router.get("/v1/ping", PingController.index);

export default router;